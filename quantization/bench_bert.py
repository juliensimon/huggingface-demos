import torch
from transformers import BertTokenizer, BertModel
import intel_extension_for_pytorch as ipex
import time,os

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
orig_model = BertModel.from_pretrained("bert-base-uncased").to(device=device)
orig_model.eval()
text = "Replace me by any text you'd like. " * 12 
print(f"Sequence length: {len(text)}")
encoded_input = tokenizer(text, return_tensors='pt').to(device=device)

def bench(model, input, n=1000):
    with torch.no_grad():
        # Warmup
        model(**encoded_input)
        start = time.time()
        for _ in range(n):
            model(**input)
        end = time.time()
        return (end - start) * 1000 / n

def print_size_of_model(model):
    torch.save(model.state_dict(), "temp.p")
    print('Size (MB):', os.path.getsize("temp.p")/1e6)
    os.remove('temp.p')

print_size_of_model(orig_model)
print(f"Average time: {bench(orig_model, encoded_input):.2f} ms")

# Dynamic PTQ

quantized_model = torch.quantization.quantize_dynamic(
    orig_model, {torch.nn.Linear}, dtype=torch.qint8
)
print(quantized_model)
print_size_of_model(quantized_model)
print(f"Average time dynamic PTQ: {bench(quantized_model, encoded_input):.2f} ms")

