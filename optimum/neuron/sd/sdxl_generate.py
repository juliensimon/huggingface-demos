import time

from optimum.neuron import NeuronStableDiffusionXLPipeline

model_id = "./sdxl_neuron"
input_shapes = {"batch_size": 1, "height": 1024, "width": 1024}

device_ids = [0, 1]
# device_ids = [0,1,2,3]

stable_diffusion = NeuronStableDiffusionXLPipeline.from_pretrained(
    model_id, export=False, **input_shapes, device_ids=device_ids
)

prompt = "Astronauts exploring a dark and hostile asteroid landscape, nebulaand black hole in starlit sky, cold color palette, muted colors, detailed, 8k"

# Warmup
images = stable_diffusion(prompt, num_inference_steps=25)

num_iterations = 10

tick = time.time()
for i in range(0, num_iterations):
    images = stable_diffusion(prompt, num_inference_steps=25)
    images[0][0].save(f"image{i}.png")
tock = time.time()

print(f"Average time: {round((tock-tick)/num_iterations, 2)} seconds")
